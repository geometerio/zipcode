Zipcode
=======

Look up information about US zipcodes.

## Installation

```elixir
def deps do
  [
    {:zipcode, "~> 0.1.0"}
  ]
end
```
