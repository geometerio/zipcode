defmodule ZipcodeTest do
  use ExUnit.Case
  doctest Zipcode

  describe "to_state" do
    test "returns a US state abbreviation based on the first 3 digits" do
      assert Zipcode.to_state("16400") == {:ok, "PA"}
      assert Zipcode.to_state("22609") == {:ok, "VA"}
      assert Zipcode.to_state("29609") == {:ok, "SC"}
      assert Zipcode.to_state("99387") == {:ok, "WA"}
    end

    test "returns error when zip code does not match to state" do
      assert Zipcode.to_state("00000") == {:error, :not_found}
      assert Zipcode.to_state("ARRGH") == {:error, :bad_format}
      assert Zipcode.to_state("1") == {:error, :bad_format}
    end
  end
end
