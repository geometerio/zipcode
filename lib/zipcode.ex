defmodule Zipcode do
  @moduledoc """
  Documentation for `Zipcode`.
  """

  @doc """
  Given a zipcode, returns a US state abbreviation.

  * https://en.wikipedia.org/wiki/List_of_ZIP_Code_prefixes
  * https://stevemorse.org/jcal/zip.html

  ## Examples

      iex> Zipcode.to_state("10400")
      {:ok, "NY"}

      iex> Zipcode.to_state("20299")
      {:ok, "DC"}

      iex> Zipcode.to_state("00200")
      {:error, :not_found}

  """
  def to_state(<<prefix::binary-size(3), _rest::binary-size(2)>>) do
    if String.match?(prefix, ~r|\d{3}|) do
      prefix
      |> String.to_integer()
      |> Zipcode.Prefixes.get()
    else
      {:error, :bad_format}
    end
  end

  def to_state(_), do: {:error, :bad_format}
end
