#!/usr/bin/env sh

source "bin/_support/cecho.sh"

localcopy() {
  content="$1"

  if command -v pbcopy &> /dev/null; then
    echo -n "$content" | pbcopy
  elif command -v xclip &> /dev/null; then
    echo "$content" | xclip -selection clipboard
  else
    cecho -n --yellow "You're missing a suitable copy command; here's the content I tried to copy:"
    cecho -n --yellow "$content"
  fi
}

darwin() {
  uname -a | grep "Darwin"
}

check() {
  description=$1
  command=$2
  remedy=$3

  cecho -n --cyan "[checking] ${description}" --white "... "

  eval "${command} > .doctor.out 2>&1"

  if [ $? -eq 0 ]; then
    cecho --green "OK"
    return 0
  else
    cecho --red "FAILED"
    cat .doctor.out
    echo
    cecho --cyan "Possible remedy: " --yellow "${remedy}"
    cecho --cyan "(it's in the clipboard)"
    localcopy "$remedy"
    exit 1
  fi
}

xcheck() {
  description=$1
  command=$2
  remedy=$3

  cecho -n --yellow "[checking] ${description}" --white "... "
  cecho --yellow "SKIPPED"
  return 0
}
