defmodule Zipcode.MixProject do
  use Mix.Project

  @version "0.1.0"

  def project do
    [
      app: :zipcode,
      deps: deps(),
      description: description(),
      elixir: "~> 1.10",
      package: package(),
      start_permanent: Mix.env() == :prod,
      version: @version
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:mix_audit, "> 0.0.0", only: :dev, runtime: false}
    ]
  end

  defp description do
    """
    Look up information about US zipcodes.
    """
  end

  defp package do
    [
      files: ["lib", "priv", "mix.exs", "README.md", "LICENSE.md"],
      maintainers: ["Geometer LLC", "Eric Saxby"],
      licenses: ["MIT"],
      links: %{
        GitLab: "https://gitlab.com/geometerio/zipcode"
      }
    ]
  end
end
